import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Modal } from 'react-bootstrap'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Stack,
  Pagination,
} from '@mui/material'

export const User = () => {
  // set các thông tin của user là 1 đối tượng với các thông số như sau
  const [userObj, setUserObj] = useState({
    fullName: '',
    phone: '',
    email: '',
    photoURL: '',
    address: '',
  })

  // sét trạng thái id của user ban đầu là rỗng
  const [userId, setUserId] = useState('')
  // sét trạng thái để check add hoặc update user nếu false là add user còn nếu true là update user
  const [modalAddState, setModalAddState] = useState(false)
  // sét trạng thái để hiển thị modal add user
  const [show, setShow] = useState(false)
  // hàm này thực hiện đóng modal lại
  const handleClose = () => {
    setShow(false)
  }

  // hàm này thực hiện mở modal lên
  const handleShow = () => {
    setShow(true)
  }

  // sét trạng thái của modal hiển thị thông báo add or update thành công
  const [showNoti, setShowNoti] = useState(false)

  // hàm này thực hiện việc đóng modal thông báo
  const handleCloseNoti = () => {
    setShowNoti(false)
  }

  // hàm này thực hiện việc mở modal thông báo lên
  const handleShowNoti = () => {
    setShowNoti(true)
  }

  // sét phần trạng thái để thông báo lỗi nếu để trống thông tin của user trên form nhập
  // mặc định ban đầu để là ko có lỗi
  const [nameErr, setNameErr] = useState(false)
  const [phoneErr, setPhoneErr] = useState(false)
  const [emailErr, setEmailErr] = useState(false)
  const [addressesErr, setAddressesErr] = useState(false)

  // set list user ban đầu là 1 mảng rỗng để gán lại giá trị sau khi gọi api thành công
  const [allUser, setAllUser] = useState([])
  const [userListPage, setUserListListPage] = useState([])

  // sét trạng thái ban đầu của select filter là all để trả ra tất cả user
  const [selectFil, setSelectFil] = useState('all')
  // sét trạng thái trên ô input để nhập lọc fiter user là rỗng
  const [inputFil, setInputFil] = useState('')
  // thực hiện gọi api để lấy toàn bộ user về và gán lại giá trị cho allUser
  useEffect(() => {
    const callApiGetAllUsers = async () => {
      const userList = await axios.get('http://localhost:5000/customer')
      setAllUser(userList.data.Customer)
    }
    callApiGetAllUsers()
  }, [showNoti])

  // hàm này để thực hiện việc mở modal add user và gán lại giá trị cho userObj
  const handleOpenModelAddUser = () => {
    setUserObj({
      fullName: '',
      phone: '',
      email: '',
      photoURL: '', // có thể nhập hoặc không tùy vào user
      address: '',
    })
    // ban đầu mặc định các ô input đều có dữ liệu user nên sẽ ko hiển thị cảnh báo
    setNameErr(false)
    setPhoneErr(false)
    setEmailErr(false)
    setAddressesErr(false)
    setModalAddState(false) // false là trạng thái cho việc add user
    handleShow() // mở modal add user lên
  }

  // hàm này thực hiện việc validate thông tin và gọi api để thêm mới user
  const handleAddUser = async () => {
    // tiến hành validate kiểm tra input
    if (userObj.fullName === '') {
      setNameErr(true)
      return
    }
    if (userObj.phone === '') {
      setPhoneErr(true)
      return
    }
    if (userObj.email === '') {
      setEmailErr(true)
      return
    }
    if (userObj.address === '') {
      setAddressesErr(true)
      return
    }

    // nếu các input đã có thông tin tiếp tục gọi api để check xem email hay phone đã tồn tại chưa
    const checkEmail = await axios.get(
      'http://localhost:5000/customer-email?email=' + userObj.email,
    )
    if (checkEmail.data.customer !== null) {
      setEmailErr(true)
      return
    }
    const checkPhone = await axios.get(
      'http://localhost:5000/customer-phone?phone=' + userObj.phone,
    )
    if (checkPhone.data.customer !== null) {
      setPhoneErr(true)
      return
    }

    // nếu tất cả các thông tin đã thỏa mãn thì gọi api để tạo mới user
    await axios.post('http://localhost:5000/customer', userObj)
    handleShowNoti()
    handleClose()
  }

  // hàm này thực hiện việc check để hiển thị phần modal update user lên qua check
  const handleShowModalUpdate = (e) => {
    setNameErr(false)
    setPhoneErr(false)
    setEmailErr(false)
    setAddressesErr(false)
    setModalAddState(true)
    setUserId(e._id)
    setUserObj({
      fullName: e.fullName,
      phone: e.phone,
      email: e.email,
      photoURL: e.photoURL,
      address: e.address,
    })
    handleShow()
  }

  // hàm này thực hiện việc gọi api để update user
  const handleUpdate = async () => {
    // kiểm tra email hay phone có chưa và không trùng với id của user được click
    const checkEmail = await axios.get(
      'http://localhost:5000/customer-email?email=' + userObj.email,
    )
    if (checkEmail.data.customer !== null && checkEmail.data.customer._id !== userId) {
      setEmailErr(true)
      return
    }
    const checkPhone = await axios.get(
      'http://localhost:5000/customer-phone?phone=' + userObj.phone,
    )
    if (checkPhone.data.customer !== null && checkPhone.data.customer._id !== userId) {
      setPhoneErr(true)
      return
    }
    // nếu đã thỏa mãn gọi api cập nhật user
    await axios.put('http://localhost:5000/customer-update?id=' + userId, userObj)
    handleShowNoti()
    handleClose()
  }

  // hàm này thực hiện việc lọc user theo lựa chọn trên select và value trên input
  const handleFilterUser = async () => {
    if (selectFil === 'all' && inputFil === '') {
      const userList = await axios.get('http://localhost:5000/customer')
      setAllUser(userList.data.customer)
      return
    }
    if (selectFil === 'name') {
      const listFilter = allUser.filter((user) => {
        return user.fullName.toLowerCase().includes(inputFil.toLowerCase())
      })
      setAllUser(listFilter)
      return
    }
    if (selectFil === 'phone') {
      const listFilter = allUser.filter((user) => {
        return user.phone.toLowerCase().includes(inputFil.toLowerCase())
      })
      setAllUser(listFilter)
      return
    }
  }

  // thực hiện phân trang
  // số trang bắt đầu từ 1
  const [page, setPage] = useState(1)
  // set số sản phẩm trên mỗi trang là 8
  const limit = 8
  // sản phẩm bắt đầu từ vị trí 0 trong data trả về từ api
  const [noPage, setNoPage] = useState(0)

  // hàm thay đổi khi load trang set lại value cho page
  const changeHandler = (event, value) => {
    setPage(value)
  }

  // thực hiện phân trang
  useEffect(() => {
    const pagiantion = () => {
      setNoPage(Math.ceil(allUser.length / limit))
      let result = allUser.slice((page - 1) * limit, page * limit)
      setUserListListPage(result)
    }
    pagiantion()
  }, [page, allUser])
  return (
    <div className="product-container">
      <div className="product-filter">
        <div className="search-container">
          <div className="form-container">
            <select className="form-select" onChange={(e) => setSelectFil(e.target.value)}>
              <option value="all">All</option>
              <option value="name">User name</option>
              <option value="phone">Phone</option>
            </select>
            <input
              type="text"
              className="form-input-filter"
              onChange={(e) => setInputFil(e.target.value)}
            />
            <div className="form-border-filter"></div>
            <div className="form-icon" onClick={handleFilterUser}>
              Lọc
            </div>
          </div>
        </div>
      </div>
      <button className="btn-add-new-user" onClick={handleOpenModelAddUser}>
        Thêm tài khoản
      </button>
      <Grid container>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  STT
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Full name
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Email
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Phone
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Address
                </TableCell>
                {/* <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Avarta
                </TableCell> */}
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody style={{ border: '1px solid #f7e7ce' }}>
              {userListPage.map((element, index) => (
                <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell
                    component="th"
                    scope="row"
                    align="center"
                    style={{ border: '1px solid #f7e7ce' }}
                  >
                    {index + 1}
                  </TableCell>
                  <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                    {element.fullName}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.email}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.phone}
                  </TableCell>

                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.address}
                  </TableCell>
                  {/* <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.photoURL}
                  </TableCell> */}

                  <TableCell align="center">
                    <button
                      className="btn-edit-order"
                      onClick={() => handleShowModalUpdate(element)}
                    >
                      <span className="fas fa-pen" />
                    </button>
                    {/* <Button
                      style={{ borderRadius: '10px', marginLeft: 5 }}
                      onClick={() => handleShowModalUpdate(element)}
                    >
                      <span className="fas fa-pen" />
                    </Button> */}

                    {/* <Button
                      style={{ borderRadius: '10px' }}
                      onClick={() => handleDeleteProduct(element)}
                    >
                      <span className="fas fa-trash" />
                    </Button> */}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      {/* phân trang */}
      <div>
        <Grid
          container
          style={{
            marginTop: '20px',
            justifyContent: 'flex-end',
          }} /* container marginTop={5} marginBottom={5} */
        >
          <Stack /* spacing={2} */>
            <Pagination
              onChange={changeHandler}
              count={noPage}
              defaultPage={page}
              color="primary"
            />
          </Stack>
        </Grid>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{modalAddState ? 'Cập thật thông tin' : 'Thêm mới tài khoản'}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-add-update">
          <div className="input-container">
            <input
              className="input-control"
              placeholder=" "
              defaultValue={userObj.fullName}
              onChange={(e) => setUserObj({ ...userObj, fullName: e.target.value })}
            />
            <label className="form-label">Họ tên</label>
          </div>
          <span className={nameErr ? '' : 'span-err-name'}>Chú ý: Tên không được để trống</span>
          <div className="input-container">
            <input
              className="input-control"
              placeholder=" "
              defaultValue={userObj.email}
              onChange={(e) => setUserObj({ ...userObj, email: e.target.value })}
            />
            <label className="form-label">Email</label>
          </div>
          <span className={emailErr ? '' : 'span-err-email'}>
            Chú ý: Email không được để trống hoặc email đã tồn tại
          </span>
          <div className="input-container">
            <input
              className="input-control"
              placeholder=" "
              defaultValue={userObj.phone}
              onChange={(e) => setUserObj({ ...userObj, phone: e.target.value })}
            />
            <label className="form-label">Số điên thoại</label>
          </div>
          <span className={phoneErr ? '' : 'span-err-phone'}>
            chú ý: Số điện thoại không được để trống hoặc số điện thoại đã tồn tại
          </span>
          <div className="input-container">
            <input
              className="input-control"
              placeholder=" "
              defaultValue={userObj.address}
              onChange={(e) => setUserObj({ ...userObj, address: e.target.value })}
            />
            <label className="form-label">Địa chỉ</label>
          </div>
          <span className={addressesErr ? '' : 'span-err-address'}>
            Chú ý: Địa chỉ không được để trống
          </span>
          {/* <div className="input-container">
            <input
              className="input-control"
              placeholder=" "
              defaultValue={userObj.photoURL}
              onChange={(e) => setUserObj({ ...userObj, photoURL: e.target.value })}
            />
            <label className="form-label">Link ảnh đại diện(nếu có)</label>
          </div> */}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-close-modal" onClick={handleClose}>
            Đóng
          </button>
          <button className="btn-confirm" onClick={modalAddState ? handleUpdate : handleAddUser}>
            {modalAddState ? 'Cập nhật' : 'Thêm mới'}
          </button>
        </Modal.Footer>
      </Modal>
      <Modal show={showNoti} onHide={handleCloseNoti}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalAddState ? 'Cập nhật thành công' : 'Thêm mới tài khoản thành công'}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-confirm" onClick={handleCloseNoti}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default User
