import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Modal } from 'react-bootstrap'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Stack,
  Pagination,
} from '@mui/material'
// import { SpaRounded } from '@mui/icons-material'

export const Product = () => {
  // chuyển đổi sang số thập phân có dấu phẩy
  var decimalNumber = new Intl.NumberFormat('en-US')
  //set trạng thái ở select chọn giá trị để lọc
  const [filterKey, setFilterKey] = useState('all')
  // sét trạng thái ở ô input
  const [fil, setFil] = useState('')

  // sét trạng thái ban đầu của toàn bộ sp mà api trả về
  const [allProduct, setAllProduct] = useState([])
  const [productListPage, setProductListListPage] = useState([])
  //phần modal để xác định xem đó là add new product and update product
  const [show, setShow] = useState(false)
  // sét trạng thái của form nếu false đó là add còn true là update product
  const [stateModal, setStateModal] = useState(false)
  // hàm này để đóng modal lại
  const handleClose = () => {
    setShow(false)
  }
  // hàm này để mở modal lên
  const handleShow = () => {
    setShow(true)
  }
  //phần Modal để xác định đó là delete and notifications nếu false là notifications true là delete
  const [deleteState, setDeleteState] = useState(false)
  // modal hiển thị thông báo
  const [showNote, setShowNote] = useState(false)
  // hàm này để đóng modal lại
  const handleCloseNote = () => {
    setShowNote(false)
  }
  // hàm này để mở modal lên
  const handleShowNote = () => {
    setShowNote(true)
  }
  // phần thông báo lỗi sẽ hiển thị khi tiến hành thêm sp trên modal
  const [nameErr, setNameErr] = useState(false)
  const [typeErr, setTypeErr] = useState(false)
  const [imageErr, setImageErr] = useState(false)
  const [priceErr, setPriceErr] = useState(false)
  const [saleErr, setSaleErr] = useState(false)
  const [descriptionErr, setDescriptionErr] = useState(false)
  // sét productId là chuỗi rỗng để hệ thống tự sinh
  const [productId, setProductId] = useState('')
  // phần thông tin product sẽ gửi đi kèm api
  const [product, setProduct] = useState({
    productName: '',
    productType: '',
    productImage: [],
    productPrice: 0,
    priceSale: 0,
    productDetail: '',
  })
  //hàm này sẽ call api để load toàn bộ sp trả ra giao diện khi trang web loading xong
  useEffect(() => {
    const getAllProduct = async () => {
      const productList = await axios.get('http://localhost:5000/products')
      if (productList.data.Product !== null) {
        setAllProduct(productList.data.Product)
      }
    }
    getAllProduct()
  }, [showNote])

  // hàm này để tiến hành show modal thêm sp lên
  const btnOnAddNewClick = () => {
    setProduct({
      productName: '',
      productType: '',
      productImage: [],
      productPrice: 0,
      priceSale: 0,
      productDetail: '',
    })
    setStateModal(false)
    handleShow()
  }

  // hàm này để cho user có thể chọn đc nhiều ảnh thêm
  const handleImageChange = (e) => {
    let urlImage = e.target.value
    setProduct({ ...product, productImage: urlImage.split(',') })
  }
  //hàm này để validate các input thêm sp và gọi api thêm sp
  const handleAddClick = async () => {
    //mặc định ban đầu các input đã có thông tin sp
    setNameErr(false)
    setTypeErr(false)
    setImageErr(false)
    setPriceErr(false)
    setSaleErr(false)
    setDescriptionErr(false)
    // tiến hành validate các ô input
    if (product.productName === '') {
      setNameErr(true)
      return
    }
    if (product.productType === '') {
      setTypeErr(true)
      return
    }
    if (product.productImage.length === 0) {
      setImageErr(true)
      return
    }
    if (product.productPrice === '') {
      setPriceErr(true)
      return
    }
    if (product.priceSale === '') {
      setSaleErr(true)
      return
    }
    if (product.productDetail === '') {
      setDescriptionErr(true)
      return
    }
    // nếu thông tin validate đã thỏa mãn gọi api thêm sp
    const newProduct = await axios.post('http://localhost:5000/products', product)
    if (newProduct.data.Product !== null) {
      setDeleteState(false)
      handleShowNote()
      handleClose()
    }
  }

  // hàm này thực hiện xác định id sp sẽ đc update và mở modal lên
  const handleUpdateProduct = (element) => {
    setProductId(element._id)
    setProduct({
      productName: element.productName,
      productType: element.productType,
      productImage: element.productImage,
      productPrice: element.productPrice,
      priceSale: element.priceSale,
      productDetail: element.productDetail,
    })
    setStateModal(true)
    handleShow()
  }

  // hàm này gọi api để update sp
  const handleUpdate = async () => {
    // Btn update Modal update
    const updateProduct = await axios.put('http://localhost:5000/products/' + productId, product)
    if (updateProduct.data.Product !== null) {
      handleShowNote()
      handleClose()
    }
  }

  // hàm này xác định id sp sẽ bị xóa và mở modal lên
  const handleDeleteProduct = (e) => {
    setProductId(e._id)
    setDeleteState(true)
    handleShowNote()
  }

  // hàm này thực hiện gọi api xóa sp
  const handleDelete = async () => {
    const deleteProduct = await axios.delete('http://localhost:5000/products/' + productId)
    if (deleteProduct.data.Product !== null) {
      handleCloseNote()
    }
  }
  //hàm này thực hiện lọc sp theo value trên input đc chọn
  const handleFilter = async () => {
    if (filterKey === 'all' && fil === '') {
      const productFilter = allProduct
      setAllProduct(productFilter)
      return
    }
    if (filterKey === 'all' && fil !== '') {
      const productFilter = allProduct.filter((prod) => {
        return prod.productName.toLowerCase().includes(fil.toLowerCase())
      })
      setAllProduct(productFilter)
      return
    }
    if (filterKey === 'name' && fil !== '') {
      const productFilter = allProduct.filter((prod) => {
        return prod.productName.toLowerCase().includes(fil.toLowerCase())
      })
      setAllProduct(productFilter)
      return
    }
    if (filterKey === 'type' && fil !== '') {
      const productFilter = allProduct.filter((prod) => {
        return prod.productType.toLowerCase().includes(fil.toLowerCase())
      })
      setAllProduct(productFilter)
      return
    }
  }

  // thực hiện phân trang
  // số trang bắt đầu từ 1
  const [page, setPage] = useState(1)
  // set số sản phẩm trên mỗi trang là 8
  const limit = 8
  // sản phẩm bắt đầu từ vị trí 0 trong data trả về từ api
  const [noPage, setNoPage] = useState(0)

  // hàm thay đổi khi load trang set lại value cho page
  const changeHandler = (event, value) => {
    setPage(value)
  }

  // thực hiện phân trang
  useEffect(() => {
    const pagiantion = () => {
      setNoPage(Math.ceil(allProduct.length / limit))
      let result = allProduct.slice((page - 1) * limit, page * limit)
      setProductListListPage(result)
    }
    pagiantion()
  }, [page, allProduct])
  return (
    <div className="product-container">
      <div className="product-filter">
        <div className="search-container">
          <div className="form-container">
            <select className="form-select" onChange={(e) => setFilterKey(e.target.value)}>
              <option value="all">All</option>
              <option value="name">Product name</option>
              <option value="type">Product type</option>
            </select>
            <input
              type="text"
              className="form-input-filter"
              onChange={(e) => setFil(e.target.value)}
            />
            <div className="form-border-filter"></div>
            <div className="form-icon" onClick={handleFilter}>
              Lọc
            </div>
          </div>
        </div>
      </div>
      <button className="btn-add-new" onClick={btnOnAddNewClick}>
        Thêm sản phẩm mới
      </button>
      <Grid container>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  STT
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Product id
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Product name
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Product type
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Product price
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Product sale
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody style={{ border: '1px solid #f7e7ce' }}>
              {productListPage.map((element, index) => (
                <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell
                    component="th"
                    scope="row"
                    align="center"
                    style={{ border: '1px solid #f7e7ce' }}
                  >
                    {index + 1}
                  </TableCell>
                  <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                    {element._id}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.productName}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.productType}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {decimalNumber.format(element.productPrice)}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {decimalNumber.format(element.priceSale)}
                  </TableCell>

                  <TableCell align="center">
                    <button
                      className="btn-update-product"
                      style={{ borderRadius: '10px', marginLeft: 5 }}
                      onClick={() => handleUpdateProduct(element)}
                    >
                      <span className="fas fa-pen" />
                    </button>
                    <button
                      className="btn-delete-product"
                      onClick={() => handleDeleteProduct(element)}
                    >
                      <span className="fas fa-trash" />
                    </button>
                    {/* <Button
                      style={{ borderRadius: '10px' }}
                      onClick={() => handleDeleteProduct(element)}
                    >
                      <span className="fas fa-trash" />
                    </Button> */}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      {/* phân trang */}
      <div>
        <Grid
          container
          style={{
            marginTop: '20px',
            justifyContent: 'flex-end',
          }} /* container marginTop={5} marginBottom={5} */
        >
          <Stack /* spacing={2} */>
            <Pagination
              onChange={changeHandler}
              count={noPage}
              defaultPage={page}
              color="primary"
            />
          </Stack>
        </Grid>
      </div>

      {/* modal add new product and update product */}
      <Modal size={'lg'} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{stateModal ? 'Cập nhật' : 'Thêm mới sản phẩm'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-container-add">
            <div className="form-add">
              <label>Product name</label>
              <input
                defaultValue={stateModal ? product.productName : ''}
                type="text"
                onChange={(e) => setProduct({ ...product, productName: e.target.value })}
              />
            </div>
            <span className={nameErr ? '' : 'span-product-name'}>Product name not null</span>
            <div className="form-add">
              <label>Product type</label>
              <input
                defaultValue={stateModal ? product.productType : ''}
                type="text"
                onChange={(e) => setProduct({ ...product, productType: e.target.value })}
              />
            </div>
            <span className={typeErr ? '' : 'span-product-type'}>Product type not null</span>
            <div className="form-add">
              <label>Product image</label>
              <textarea
                defaultValue={stateModal ? product.productImage : ''}
                name="description"
                onChange={(e) => handleImageChange(e)}
              ></textarea>
            </div>
            <span>
              {imageErr
                ? 'hình ảnh không được để trống'
                : 'thêm ảnh sản phẩm có đuôi như: jpg, png'}
            </span>
            <div className="form-add">
              <label>Product price</label>
              <input
                defaultValue={stateModal ? product.productPrice : ''}
                type="number"
                onChange={(e) => setProduct({ ...product, productPrice: e.target.value })}
              />
            </div>
            <span className={priceErr ? '' : 'span-product-price'}>Product price not null</span>
            <div className="form-add">
              <label>Product sale</label>
              <input
                defaultValue={stateModal ? product.priceSale : ''}
                type="number"
                onChange={(e) => setProduct({ ...product, priceSale: e.target.value })}
              />
            </div>
            <span className={saleErr ? '' : 'span-product-sale'}>Product sale not null</span>
            <div className="form-add">
              <label>Description</label>
              <textarea
                defaultValue={stateModal ? product.productDetail : ''}
                name="description"
                onChange={(e) => setProduct({ ...product, productDetail: e.target.value })}
              ></textarea>
            </div>
            <span className={descriptionErr ? '' : 'span-product-descrip'}>
              Description not null
            </span>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-close-modal" onClick={handleClose}>
            Đóng
          </button>
          <button className="btn-confirm" onClick={stateModal ? handleUpdate : handleAddClick}>
            {stateModal ? 'Cập nhật' : 'Thêm mới'}
          </button>
        </Modal.Footer>
      </Modal>
      {/* modal notifications  */}
      <Modal show={showNote} onHide={handleCloseNote}>
        <Modal.Header closeButton>
          <Modal.Title>{deleteState ? 'Chú ý' : 'Thông tin'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {deleteState
            ? 'Bạn có muốn xóa sản phẩm này không?'
            : stateModal
            ? 'Cập nhật thành công'
            : 'Thêm sản phẩm mới thành công'}
        </Modal.Body>
        <Modal.Footer>
          {deleteState ? (
            <button className="btn-close-modal" onClick={handleCloseNote}>
              Đóng
            </button>
          ) : (
            ''
          )}

          <button className="btn-confirm" onClick={deleteState ? handleDelete : handleCloseNote}>
            {deleteState ? ' Xóa' : 'Thành công!'}
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default Product

{
  /* <div>
<Table striped bordered hover>
          <thead>
            <tr>
              <th>STT</th>
              <th>Product id</th>
              <th>Product name</th>
              <th>Product type</th>
              <th>Product price</th>
              <th>Product sale</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {allProduct.map((element, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{element._id}</td>
                <td>{element.productName}</td>
                <td>{element.productType}</td>
                <td>{decimalNumber.format(element.productPrice)}</td>
                <td>{decimalNumber.format(element.priceSale)}</td>
                <td style={{ textAlign: "center" }}>
                  <CIcon
                    title="Update"
                    size={"lg"}
                    style={{ color: "#3ea6ff", cursor: "pointer" }}
                    icon={cilPencil}
                    onClick={() => handleUpdateProduct(element)}
                  />
                  &nbsp;
                  <CIcon
                    title="Delete"
                    size={"lg"}
                    style={{ color: "#cc0000", cursor: "pointer" }}
                    icon={cilX}
                    onClick={() => handleDeleteProduct(element)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
</div> */
}
