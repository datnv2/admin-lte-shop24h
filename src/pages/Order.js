import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Modal } from 'react-bootstrap'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Stack,
  Pagination,
} from '@mui/material'

export const Order = () => {
  //sét trạng thái ở select filter khi tìm kiếm và mặc định để là all
  const [filterKey, setFilterKey] = useState('all')
  // sét trạng thái của input để cho nhập dữ liệu  lọc
  const [inputFil, setInputFil] = useState('')

  // sét thông tin để tạo mới 1 order detail gửi kèm khi tạo mới
  const [newOrderDetail, setNewOrderDetail] = useState({
    orderId: '',
    productId: '',
    quantity: '',
    priceEach: '',
  })

  // sét trạng thái của form để check xem là add hay update order detail
  // nếu là false đó là add order detail còn nếu là true đó là update order detail
  const [stateOrderDetail, setStateOrderDetail] = useState(false)

  // sét trạng thái để đóng mở modal thông báo
  const [showNoti, setShowNoti] = useState(false)
  // hàm này thực hiện việc đóng modal
  const handleCloseNoti = () => setShowNoti(false)

  // hàm này để mở modal lên
  const handleShowNoti = () => setShowNoti(true)

  // sét trạng thái để check xem là sẽ tạo mới order hay tạo mới order detail
  // nếu false là create order nếu true là create order detail
  const [orderCreateState, setOrderCreateState] = useState(false)

  // sét trạng thái của form để thực hiện thêm mới order hoặc update order
  // nếu là false đó là add order còn nếu là false đó là update order
  const [formMode, setFormMode] = useState(false)
  // sét trạng thái để đóng mở modal order detail
  const [showOrderDetail, setShowOrderDetail] = useState(false)
  // hàm này để thực hiện việc đóng modal order detail lại
  const handleCloseOrderDetail = () => setShowOrderDetail(false)

  // hàm này để mở modal order detail lên
  const handleShowOrderDetail = () => setShowOrderDetail(true)

  // set trạng thái để đóng mở modal add order và update order
  // nếu false là add order còn true là update order
  const [show, setShow] = useState(false)
  // hàm này thực hiện việc đóng modal lại
  const handleClose = () => setShow(false)
  // hàm này thực hiện việc mở modal lên
  const handleShow = () => setShow(true)

  // sét trạng thái của modal để tìm user trước khi add new order
  const [showUser, setShowUser] = useState(false)
  // hàm này thực hiện việc đóng modal show find user lại
  const handleCloseUser = () => setShowUser(false)
  // hàm này thực hiện việc mở modal find user lên
  const handleShowUser = () => setShowUser(true)

  // sét thông tin của orderdetail gửi đi kèm api là 1 đối tượng
  const [orderDetail, setOrderDetail] = useState({
    orderId: '',
    productId: '',
    quantity: 1,
    priceEach: 0,
  })

  // sét trạng thái để báo lỗi khi login user
  // nếu false là đúng còn true là lỗi
  const [customerErr, setCustomerErr] = useState(false)
  // sét các thông tin khi tạo 1 order mới là 1 đối tượng chứa các thông tin ban đầu là rỗng
  const [orderObj, setOrderObj] = useState({
    customerId: '',
    orderDate: '',
    requiredDate: '',
    shippedDate: '',
    note: '',
    status: 0,
  })

  // sét order detai list là 1 mảng rỗng để gán lại giá trị sau khi gọi api
  const [orderDetailList, setOrderDetailList] = useState([])

  //set order detail id là 1 đối tượng và ban đầu là rỗng
  const [orderDetailModal, setOrderDetailModal] = useState({})

  //set id của order khi update ban đầu là rỗng để gán lại giá trị sau
  const [orderId, setOrderId] = useState('')

  // set các products ban đầu là 1 obj rỗng và sẽ gán lại giá trị sau khi gọi api
  const [productList, setProductList] = useState({})

  // sét trạng thái để lấy được thông tin của customer
  const [customerObj, setCustomerObj] = useState({})
  // sét trạng thái để nhập thông tin người dùng theo sđt or email trước khi tạo order
  const [customerFilter, setCustomerFilter] = useState('')

  // set list order ban đầu là 1 mảng rỗng để gán lại giá trị sau khi gọi api
  const [allOrder, setAllOrder] = useState([])
  const [orderListPage, setOrderListListPage] = useState([])

  // sét trạng thái cho modal show order detail
  const [showDetail, setShowDetail] = useState(false)

  // hàm này thực hiện việc đóng modal order detail
  const handleCloseDetail = () => setShowDetail(false)

  // hàm này thực hiện việc mở modal order detail
  const handleOpenDetail = () => setShowDetail(true)

  // thực hiện việc gọi api load toàn bộ order và sản phẩm ra khi trang web load xong
  useEffect(() => {
    const callApiGetAllOrderAndProducts = async () => {
      // gọi api để lấy toàn bộ order và gán lại vào all order
      const orderList = await axios.get('http://localhost:5000/order')
      setAllOrder(orderList.data.Order)
      // gọi api để lấy toàn bộ sản phẩm về
      const listProduct = await axios.get('http://localhost:5000/products')
      setProductList(listProduct.data.Product)
    }
    callApiGetAllOrderAndProducts()
  }, [showNoti])

  //hàm này thực hiện việc mở modal add order dựa vào trạng thái formMode
  const handelShowModalAdd = () => {
    // sét lại các thông tin của order
    setOrderObj({
      customerId: '',
      orderDate: '',
      requiredDate: '',
      shippedDate: '',
      note: '',
      status: 0,
    })
    setFormMode(false) // trạng thái form là add
    setCustomerErr(false) // lỗi khi nhập thông tin user trước khi vào order là ko có
    handleShowUser() // mở modal check user lên
  }

  // hàm này thực hiện việc tìm user trên csdl xem đã có chưa trước khi vào order
  const handleFindUser = async () => {
    // gọi api để check thông tin user theo value nhập trên inputs
    const customer = await axios.get('http://localhost:5000/customer-by?key=' + customerFilter)
    setCustomerObj(customer.data.customer)
    handleShow() // mở modal add order lên
    handleCloseUser() // đóng modal check user
    setOrderCreateState(false) // trạng thái để add order
    setOrderObj({
      ...orderObj, // giữ nguyên các thông tin của orderobj và kèm thêm với id của customer
      customerId: customer.data.customer._id,
    })
    setCustomerErr(true)
  }

  // hàm này để thực hiện việc tiếp tục vào phần thông tin để nhập tạo order
  const backCreateOrder = () => {
    setOrderCreateState(false)
  }

  //hàm này thực hiện việc gọi api tạo order mới
  const handleCreateOrder = async () => {
    const createOrder = await axios.post('http://localhost:5000/order', orderObj)
    setOrderDetail({ ...orderDetail, orderId: createOrder.data.Order._id })
    setOrderCreateState(true)
    setOrderDetailList([])
  }

  // hàm này để gọi api tạo mới order detail list
  const handleCreateOrderDetail = () => {
    // lấy danh sách order detail list lọc qua các phần tử của mảng
    orderDetailList.map(async (createOrderDetail) => {
      await axios.post('http://localhost:5000/order-detail', createOrderDetail)
    })
    handleShowNoti()
    handleClose()
  }

  // hàm này thực hiện việc filter data product theo id được chọn trên select
  const handleChangeSelectValue = (e) => {
    const product = productList.filter((prod) => prod._id === e.target.value)
    setOrderDetail({
      ...orderDetail,
      priceEach: product[0].productPrice,
      productId: product[0]._id,
    })
  }

  // hàm này thực hiện việc mở modal update order lên theo trạng thái formode và hiển thị thông tin order lên
  const handleShowModalUpdate = async (e) => {
    // gọi api để lấy toàn bộ sản phẩm về
    const listProduct = await axios.get('http://localhost:5000/products')
    setProductList(listProduct.data.Product)
    // sét lại trạng thái form
    setFormMode(true)
    setOrderId(e._id) // theo id của order
    setOrderCreateState(false)
    setOrderObj({
      // thông tin của order user
      customerId: e.customerId,
      orderDate: e.orderDate,
      requiredDate: e.requiredDate,
      shippedDate: e.shippedDate,
      note: e.note,
      status: e.status,
    })
    // gọi api để lấy thông tin customer order theo customerid
    const customerOrder = await axios.get('http://localhost:5000/customer/' + e.customerId)
    setCustomerObj(customerOrder.data.Customer)
    handleShow()
  }

  //  hàm này thực hiện việc gọi api để update order theo id được click
  const handleUpdateOrder = async () => {
    await axios.put('http://localhost:5000/order/' + orderId, orderObj)
    handleShowNoti()
    handleClose()
  }
  // hàm này thực hiện viêc add thêm sản phẩm vào order detail khi sp được chọn và bấm nút add thêm
  const addProductToOrderDetail = () => {
    setOrderDetailList([...orderDetailList, orderDetail])
  }

  // hàm này thực hiện việc mở modal order detail theo id lên
  const handleShowModalOrderDetail = async (e) => {
    setOrderDetailList([])
    const orderShow = await axios.get('http://localhost:5000/order-detail-id/' + e._id)
    setOrderDetailList(orderShow.data.orderDetail)
    handleOpenDetail()
  }

  // hàm này thực hiện việc mở modal order detail lên
  const handleUpdateOrderDetail = (e) => {
    // trạng thái là false
    setStateOrderDetail(false)
    // sét lại orderdetail id
    setOrderDetailModal(e)
    // mở modal lên
    handleShowOrderDetail()
  }

  // hàm này thực hiện việc update order detail
  const handleOrderDetail = async () => {
    await axios.put('http://localhost:5000/order-detail/' + orderDetailModal._id, orderDetailModal)
    setOrderDetailList([])
    // gọi api để hiển thị thông tin lên modal
    const orderShow = await axios.get(
      'http://localhost:5000/order-detail-id/' + orderDetailModal.orderId,
    )
    setOrderDetailList(orderShow.data.orderDetail)
    handleShowNoti()
    handleCloseOrderDetail()
  }

  // hàm này thực hiện việc xóa order detail
  const handleDeleteOrderDetail = async (e) => {
    await axios.delete('http://localhost:5000/order-detail/' + e._id)
    setOrderDetailList([])
    const orderShow = await axios.get('http://localhost:5000/order-detail-id/' + e.orderId)
    setOrderDetailList(orderShow.data.orderDetail)
    handleShowNoti()
    handleCloseOrderDetail()
  }

  // hàm này để mở modal add order detail
  const handleAddOrderDetail = () => {
    // sét lại trạng thái state order detail
    setStateOrderDetail(true)
    handleShowOrderDetail()
    setNewOrderDetail({
      orderId: orderDetailList[0].orderId,
    })
  }

  // hàm thực hiện việc thay đổi value trên select chọn sp ở order detail
  const handleChangeSelectValueOrderDetail = (e) => {
    const productFil = productList.filter((product) => product._id === e)
    setNewOrderDetail({
      orderId: orderDetailList[0].orderId,
      productId: productFil[0]._id,
      quantity: productFil[0].productQuantity,
      priceEach: productFil[0].productPrice,
    })
  }

  // hàm này thực hiện việc gọi api để add new orderdetail
  const handleAddNewOrderDetail = async () => {
    const addNew = await axios.post('http://localhost:5000/order-detail/', newOrderDetail)
    if (addNew.data.OrderDetail !== null) {
      handleShowNoti()
      setOrderDetailList([])
      const orderShow = await axios.get(
        'http://localhost:5000/order-detail-id/' + addNew.data.OrderDetail.orderId,
      )
      if (orderShow.data.orderDetail !== null) {
        setOrderDetailList(orderShow.data.orderDetail)
      }
      handleCloseOrderDetail()
    }
  }

  // hàm này thực hiện lọc theo đk
  const handleFilter = async () => {
    // nếu từ khóa tìm kiếm là rỗng mặc định select filter là all thì trả ra toàn bộ order
    if (filterKey === 'all') {
      //gọi api để lấy toàn bộ order
      const orderList = await axios.get('http://localhost:5000/order')
      setAllOrder(orderList.data.Order)
      return
    }

    //nếu key được tìm là orderDate thì gọi api trả ra order date
    if (filterKey === 'orderDate' && inputFil !== null) {
      let fillterArray = []
      const orderList = await axios.get('http://localhost:5000/order')
      fillterArray = orderList.data.Order.filter((order) => {
        return order.orderDate === inputFil
      })
      setAllOrder(fillterArray)
      return
    }
    //nếu key được tìm là shippedDate thì gọi api trả ra shipped date
    if (filterKey === 'shippedDate' && inputFil !== null) {
      let fillterArray = []
      const orderList = await axios.get('http://localhost:5000/order')
      fillterArray = orderList.data.Order.filter((order) => {
        return order.shippedDate === inputFil
      })
      setAllOrder(fillterArray)
      return
    }
  }

  // thực hiện phân trang
  // số trang bắt đầu từ 1
  const [page, setPage] = useState(1)
  // set số sản phẩm trên mỗi trang là 8
  const limit = 8
  // sản phẩm bắt đầu từ vị trí 0 trong data trả về từ api
  const [noPage, setNoPage] = useState(0)

  // hàm thay đổi khi load trang set lại value cho page
  const changeHandler = (event, value) => {
    setPage(value)
  }

  // thực hiện phân trang
  useEffect(() => {
    const pagiantion = () => {
      setNoPage(Math.ceil(allOrder.length / limit))
      let result = allOrder.slice((page - 1) * limit, page * limit)
      setOrderListListPage(result)
    }
    pagiantion()
  }, [page, allOrder])

  //khai báo biến để hiển thị ngày tháng năm
  //  let today = new Date();
  //  let date =
  //    today.getDate() +
  //    "/" +
  //    parseInt(today.getMonth() + 1) +
  //    "/" +
  //    today.getFullYear();

  return (
    <div className="product-container">
      <div className="product-filter">
        <div className="search-container">
          <div className="form-container">
            <select className="form-select" onChange={(e) => setFilterKey(e.target.value)}>
              <option value="all">All</option>
              <option value="orderDate">orderDate</option>
              <option value="shippedDate">shippedDate</option>
            </select>
            <input
              type="text"
              className="form-input-filter"
              onChange={(e) => setInputFil(e.target.value)}
            />
            <div className="form-border-filter"></div>
            <div className="form-icon" onClick={handleFilter}>
              Filter
            </div>
          </div>
        </div>
      </div>
      <button className="btn-add-new-order" onClick={handelShowModalAdd}>
        Thêm đơn hàng mới
      </button>
      <Grid container>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  STT
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  CustomerId
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  OrderDate
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  RequiredDate
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  ShippedDate
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Note
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Detail
                </TableCell>
                <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody style={{ border: '1px solid #f7e7ce' }}>
              {orderListPage.map((element, index) => (
                <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell
                    component="th"
                    scope="row"
                    align="center"
                    style={{ border: '1px solid #f7e7ce' }}
                  >
                    {index + 1}
                  </TableCell>
                  <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                    {element.customerId}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.orderDate?.slice(0, 10)}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.requiredDate?.slice(0, 10)}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.shippedDate?.slice(0, 10)}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    {element.note}
                  </TableCell>
                  <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                    <button
                      className="btn-detail-order"
                      onClick={() => handleShowModalOrderDetail(element)}
                    >
                      Detail
                    </button>
                  </TableCell>

                  <TableCell align="center">
                    <button
                      className="btn-update-order-pen"
                      onClick={() => handleShowModalUpdate(element)}
                    >
                      <span className="fas fa-pen" />
                    </button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      {/* phân trang */}
      <div>
        <Grid
          container
          style={{
            marginTop: '20px',
            justifyContent: 'flex-end',
          }} /* container marginTop={5} marginBottom={5} */
        >
          <Stack /* spacing={2} */>
            <Pagination
              onChange={changeHandler}
              count={noPage}
              defaultPage={page}
              color="primary"
            />
          </Stack>
        </Grid>
      </div>

      <Modal size={'xl'} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{formMode ? 'Cập nhật đơn hàng' : 'Thêm đơn hàng mới'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {orderCreateState ? (
            <div className="modal-add-order">
              <div className="customer-order-container">
                <div className="order-header">
                  <h3>Product info</h3>
                </div>
                <div className="order-content">
                  <div className="order-form">
                    <label>Product name</label>
                    <select
                      defaultValue={orderDetail.productId}
                      onChange={(e) => handleChangeSelectValue(e)}
                    >
                      <option value="">Chọn sản phẩm</option>
                      {productList.map((element, index) => (
                        <option key={index} value={element._id}>
                          {element.productName}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="order-form">
                    <label>Quantity</label>
                    <input
                      type="number"
                      defaultValue={orderDetail.quantity}
                      onChange={(e) =>
                        setOrderDetail({
                          ...orderDetail,
                          quantity: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div className="order-form">
                    <label>Price Each </label>
                    <p>{orderDetail.priceEach} VND</p>
                  </div>
                  <button className="btn-add-product" onClick={addProductToOrderDetail}>
                    Mua &gt;{' '}
                  </button>
                </div>
              </div>
              <div className="customer-order-container">
                <div className="customer-order">
                  <div className="order-header">
                    <h3>{formMode ? 'Update Order Detail' : 'Create Order Detail'}</h3>
                  </div>
                  <div className="order-content">
                    {orderDetailList.map((element, index) => (
                      <div key={index} className="order-form-product">
                        <div className="order-form-product-header">
                          <label>Order Id</label>
                          <label>Product Id</label>
                          <label>Quantity</label>
                          <label>Price Each</label>
                        </div>
                        <div className="order-form-product-header">
                          <label className="label-orderid">{element.orderId}</label>
                          <label className="label-orderid">{element.productId}</label>
                          <label>{element.quantity}</label>
                          <label>{element.priceEach}</label>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="modal-add-order">
              <div className="customer-order-container">
                <div className="order-header">
                  <h3>Customer info</h3>
                </div>
                <div className="order-content">
                  <div className="order-form">
                    <label>Full name</label>
                    <input defaultValue={customerObj.fullName} type="text" disabled />
                  </div>
                  <div className="order-form">
                    <label>Email</label>
                    <input type="text" defaultValue={customerObj.email} disabled />
                  </div>
                  <div className="order-form">
                    <label>Phone</label>
                    <input type="text" defaultValue={customerObj.phone} disabled />
                  </div>
                  <div className="order-form">
                    <label>Address</label>
                    <input type="text" defaultValue={customerObj.address} disabled />
                  </div>
                  <div className="order-form">
                    <label>Ảnh</label>
                    <input type="text" defaultValue={customerObj.photoURL} disabled />
                  </div>
                </div>
              </div>
              <div className="customer-order-container">
                <div className="customer-order">
                  <div className="order-header">
                    <h3>{formMode ? 'Cập nhật đơn hàng' : 'Thêm đơn hàng mới'}</h3>
                  </div>
                  <div className="order-content">
                    <div className="order-form">
                      <label>Customer Id</label>
                      <input type="text" disabled defaultValue={customerObj._id} />
                    </div>
                    <div className="order-form">
                      <label>Order date</label>
                      <input
                        type="text"
                        defaultValue={orderObj.orderDate.slice(0, 10)}
                        onChange={(e) =>
                          setOrderObj({
                            ...orderObj,
                            orderDate: e.target.value,
                          })
                        }
                      />
                    </div>
                    <div className="order-form">
                      <label>Required Date</label>
                      <input
                        type="text"
                        defaultValue={orderObj.requiredDate.slice(0, 10)}
                        onChange={(e) =>
                          setOrderObj({
                            ...orderObj,
                            requiredDate: e.target.value,
                          })
                        }
                      />
                    </div>
                    <div className="order-form">
                      <label>Shipped date</label>
                      <input
                        type="text"
                        defaultValue={orderObj.shippedDate.slice(0, 10)}
                        onChange={(e) =>
                          setOrderObj({
                            ...orderObj,
                            shippedDate: e.target.value,
                          })
                        }
                      />
                    </div>
                    <div className="order-form">
                      <label>Note</label>
                      <textarea
                        defaultValue={orderObj.note}
                        onChange={(e) =>
                          setOrderObj({
                            ...orderObj,
                            note: e.target.value,
                          })
                        }
                      ></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn-close-modal"
            onClick={orderCreateState ? backCreateOrder : handleClose}
          >
            {orderCreateState ? 'Quay lại' : 'Đóng'}
          </button>
          {formMode ? (
            <button
              className="btn-confirm"
              onClick={orderCreateState ? handleUpdateOrderDetail : handleUpdateOrder}
            >
              {orderCreateState ? 'Cập nhật' : 'Cập nhật'}
            </button>
          ) : (
            <button
              className="btn-confirm"
              onClick={orderCreateState ? handleCreateOrderDetail : handleCreateOrder}
            >
              {orderCreateState ? 'Thêm' : 'Thêm đơn hàng'}
            </button>
          )}
        </Modal.Footer>
      </Modal>
      <Modal show={showUser} onHide={handleCloseUser}>
        <Modal.Header closeButton>
          <Modal.Title>{formMode ? 'Cập nhật đơn hàng' : 'Thêm đơn hàng mới'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-search-user">
            <label>Nhập email hoặc số điện thoại</label>
            <input type="text" onChange={(e) => setCustomerFilter(e.target.value)} />
            <span>{customerErr ? 'Customer not found' : ''}</span>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-close-modal" onClick={handleCloseUser}>
            Đóng
          </button>
          <button className="btn-confirm" onClick={handleFindUser}>
            Tiếp
          </button>
        </Modal.Footer>
      </Modal>
      <Modal show={showNoti} onHide={handleCloseNoti}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>{formMode ? 'Thành công' : 'Thành công'}</Modal.Body>
        <Modal.Footer>
          <button className="btn-confirm" onClick={handleCloseNoti}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
      {/* modal order detail */}
      <Modal size={'xl'} show={showDetail} onHide={handleCloseDetail}>
        <Modal.Header closeButton>
          <Modal.Title>Order Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <button className="btn-add-new" onClick={handleAddOrderDetail}>
            Thêm chi tiết đơn hàng
          </button>
          <Grid container>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      STT
                    </TableCell>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      Order Id
                    </TableCell>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      Product Id
                    </TableCell>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      Quantity
                    </TableCell>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      Price Each
                    </TableCell>
                    <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                      Action
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: '1px solid #f7e7ce' }}>
                  {orderDetailList.map((element, index) => (
                    <TableRow
                      key={index}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        style={{ border: '1px solid #f7e7ce' }}
                      >
                        {index + 1}
                      </TableCell>
                      <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                        {element.orderId}
                      </TableCell>
                      <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                        {element.productId}
                      </TableCell>
                      <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                        {element.quantity}
                      </TableCell>
                      <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                        {element.priceEach}
                      </TableCell>
                      <TableCell style={{ border: '1px solid #f7e7ce' }} align="center">
                        <button
                          className="btn-detail-order-detail"
                          onClick={() => handleUpdateOrderDetail(element)}
                        >
                          <span className="fas fa-pen" />
                        </button>
                        <button
                          className="btn-update-order-pen-delete"
                          onClick={() => handleDeleteOrderDetail(element)}
                        >
                          <span className="far fa-trash-alt" />
                        </button>
                      </TableCell>

                      {/* <TableCell align="center" style={{ border: '1px solid #f7e7ce' }}>
                        <button
                          className="btn-update-order-pen"
                          onClick={() => handleDeleteOrderDetail(element)}
                        >
                          <span className="far fa-trash-alt" />
                        </button>
                      </TableCell> */}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-confirm" onClick={handleCloseDetail}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
      {/* modal order detail  */}
      <Modal show={showOrderDetail} onHide={handleCloseOrderDetail}>
        <Modal.Header closeButton>
          <Modal.Title>{stateOrderDetail ? 'Thêm mới' : 'Cập nhật'}</Modal.Title>
        </Modal.Header>
        {stateOrderDetail ? (
          <Modal.Body>
            <div className="form-add">
              <label>OrderId </label>
              <input type="text" disabled defaultValue={newOrderDetail.orderId} />
            </div>
            <div className="form-add">
              <label>Product name</label>
              <select onChange={(e) => handleChangeSelectValueOrderDetail(e.target.value)}>
                {productList.map((ele, index) => (
                  <option key={index} value={ele._id}>
                    {ele.productName}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-add">
              <label>Quantity</label>
              <input
                type="text"
                defaultValue={newOrderDetail.quantity}
                onChange={(e) =>
                  setNewOrderDetail({
                    ...newOrderDetail,
                    quantity: e.target.value,
                  })
                }
              />
            </div>
            <div className="form-add">
              <label>Price Each</label>
              <input type="text" disabled defaultValue={newOrderDetail.priceEach} />
            </div>
          </Modal.Body>
        ) : (
          <Modal.Body>
            <div className="form-add">
              <label>OrderId</label>
              <input type="text" disabled defaultValue={orderDetailModal.orderId} />
            </div>
            <div className="form-add">
              <label>ProductId</label>
              <input type="text" disabled defaultValue={orderDetailModal.productId} />
            </div>
            <div className="form-add">
              <label>Quantity</label>
              <input
                type="text"
                defaultValue={orderDetailModal.quantity}
                onChange={(e) =>
                  setOrderDetailModal({
                    ...orderDetailModal,
                    quantity: e.target.value,
                  })
                }
              />
            </div>
            <div className="form-add">
              <label>Price Each</label>
              <input type="text" disabled defaultValue={orderDetailModal.priceEach} />
            </div>
          </Modal.Body>
        )}
        <Modal.Footer>
          <button className="btn-cancel" onClick={handleCloseOrderDetail}>
            Đóng
          </button>
          <button
            className="btn-confirm"
            onClick={stateOrderDetail ? handleAddNewOrderDetail : handleOrderDetail}
          >
            {stateOrderDetail ? 'Thêm mới' : 'Cập nhật'}
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
export default Order

{
  /* <div>
<Table striped bordered hover>
        <thead>
          <tr>
            <th>STT</th>
            <th>customerId</th>
            <th>orderDate</th>
            <th>requiredDate</th>
            <th>shippedDate</th>
            <th>note</th>
            <th>Detail</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {allOrder.map((element, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{element.customerId}</td>
              <td>{element.orderDate?.slice(0, 10)}</td>
              <td>{element.requiredDate?.slice(0, 10)}</td>
              <td>{element.shippedDate?.slice(0, 10)}</td>
              <td>{element.note}</td>
              <td
                className="order-detail-table"
                onClick={() => handleShowModalOrderDetail(element)}
              >
                Order detail
              </td>
              <td style={{ textAlign: 'center' }}>
                <CIcon
                  title="Update"
                  size={'lg'}
                  style={{ color: '#3ea6ff', cursor: 'pointer' }}
                  icon={cilPencil}
                  onClick={() => handleShowModalUpdate(element)}
                />
                <CIcon
                  title="Delete"
                  size={'lg'}
                  style={{ color: '#cc0000', cursor: 'pointer' }}
                  icon={cilX}
                  onClick={() => handleDeleteOrderDetail(element)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
</div> */
}

{
  /* <div>
<Table striped bordered hover>
            <thead>
              <tr>
                <th>STT</th>
                <th>Order Id</th>
                <th>Product Id</th>
                <th>Quantity</th>
                <th>Price Each</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {orderDetailList.map((element, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{element.orderId}</td>
                  <td>{element.productId}</td>
                  <td>{element.quantity}</td>
                  <td>{element.priceEach}</td>
                  <td style={{ textAlign: 'center' }}>
                    <CIcon
                      title="Update"
                      size={'lg'}
                      style={{ color: '#3ea6ff', cursor: 'pointer' }}
                      icon={cilPencil}
                      onClick={() => handleUpdateOrderDetail(element)}
                    />
                    &nbsp;
                    <CIcon
                      title="Delete"
                      size={'lg'}
                      style={{ color: '#cc0000', cursor: 'pointer' }}
                      icon={cilX}
                      onClick={() => handleDeleteOrderDetail(element)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
</div> */
}
